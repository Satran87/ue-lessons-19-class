#pragma once
#include <iostream>
class Animal
{
public:
	Animal()=default;
	virtual ~Animal()=default;
	virtual void Voice()=0;
};

