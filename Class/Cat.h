#pragma once
#include "Animal.h"
class Cat :
	public Animal
{
public:
	Cat() = default;
	~Cat()=default;
	void Voice() override;
};

