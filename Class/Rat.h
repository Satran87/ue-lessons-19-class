#pragma once
#include "Animal.h"
class Rat :
	public Animal
{
public:
	Rat() = default;
	~Rat() = default;;
	void Voice() override;
};

