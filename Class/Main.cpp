#include <cstdlib>
#include "Animal.h"
#include "Dog.h"
#include "Cat.h"
#include "Rat.h"

int main()
{
	Animal* arr[3] = {};
	Dog* MyDog = new Dog();
	Cat* MyCat = new Cat();
	Rat* MyRat = new Rat();
	arr[0] = MyDog;
	arr[1] = MyCat;
	arr[2] = MyRat;
	for (const auto item : arr)
	{
		item->Voice();
	}
	system("pause");
	delete MyDog;
	delete MyCat;
	delete MyRat;
	return EXIT_SUCCESS;
}
